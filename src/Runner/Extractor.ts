import {TestCase} from "../Tests/TestCase";

export class Extractor
{
    /**
     * @return Function[]
     */
    public getTests(testCase: TestCase)
    {
        let tests = [];

        for (let method of Object.getOwnPropertyNames(testCase.constructor.prototype)) {
            if (method.substr(0, 5) === 'test_') {
                tests.push(method);
            }
        }

        return tests;
    }
}