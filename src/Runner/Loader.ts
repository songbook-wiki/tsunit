import {TestCase} from "../Tests/TestCase";

export class Loader
{
    private glob;
    private workingDirectory: string;

    /**
     * @param workingDirectory
     * @param glob
     */
    public constructor(workingDirectory: string, glob)
    {
        this.workingDirectory = workingDirectory;
        this.glob = glob;
    }

    /**
     * @param path
     * @return TestCase[]
     */
    public async load(path: string)
    {
        let files = await this.getFiles(this.workingDirectory + path + '**/*.ts');
        let testCases = [];

        for (let file of files) {
            let testCase = await import(file);

            testCases.push(new testCase.TestCaseTest());
        }

        return testCases;
    }

    /**
     * @return string[]
     */
    private async getFiles(path: string)
    {
        return this.glob(path);
    }
}