import {Extractor} from "./Extractor";
import {Loader} from "./Loader";
import {TestCase} from "../Tests/TestCase";
import {AssertionError} from "assert";
import {AssertionException} from "../Errors/AssertionException";
import {ExceptionNotThrownException} from "../Errors/ExceptionNotThrownException";

export class Runner
{
    private loader: Loader;
    private extractor: Extractor;

    /**
     * @param loader
     * @param extractor
     */
    constructor(loader: Loader, extractor: Extractor)
    {
        this.loader = loader;
        this.extractor = extractor;
    }

    /**
     *
     */
    async run(path) {
        let testCases: TestCase[] = await this.loader.load(path);

        for (let testCase of testCases) {
            this.runTestCase(testCase);
        }
    }

    /**
     *
     * @param testCase
     */
    private runTestCase(testCase: TestCase) {
        let tests: string[] = this.extractor.getTests(testCase);

        for (let test of tests) {
            try {
                this.runTest(testCase, test);
                process.stdout.write('.');
            } catch (e) {

                console.log(e.message);
                process.stdout.write('f');
            }
        }
    }

    /**
     * @param testCase
     * @param test
     */
    private runTest(testCase: TestCase, test: string) {
        let thrownException: Error = null;

        try {
            testCase.setUp();
            testCase[test]();
            testCase.tearDown();
        } catch (e) {
            thrownException = e;
        }

       this.validateExceptions(thrownException, testCase);
    }

    /**
     * @param thrownException
     * @param testCase
     */
    private validateExceptions(thrownException: Error, testCase: TestCase) {
        let expectedException = testCase.getExpectedException();
        let expectedMessage = testCase.getExpectedExceptionMessage();

        if (!expectedException && thrownException) {
            throw thrownException;
        }


        if (expectedException && !thrownException) {
            throw new ExceptionNotThrownException('Not thrown');
        }

        if (expectedException && thrownException) {
            if (! (thrownException instanceof expectedException)) {
                throw thrownException;
            }

            if (expectedMessage && thrownException.message !== expectedMessage) {
                throw thrownException;
            }
        }
    }
}
