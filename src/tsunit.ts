import {Loader} from "./Runner/Loader";
import {Runner} from "./Runner/Runner";
import {Extractor} from "./Runner/Extractor";

let glob = require('glob-promise');

function main() {
    let runner = new Runner(
        new Loader(process.cwd() + '/', glob),
        new Extractor()
    );

    runner.run('tests/');
}

main();


