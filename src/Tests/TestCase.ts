import {AssertionException} from "../Errors/AssertionException";

export class TestCase
{
    private expectedException: Function;
    private expectedExceptionMessage: string;

    /**
     *
     */
    public setUp() {
        this.expectedException = null;
        this.expectedExceptionMessage = null;
    }

    /**
     *
     */
    public tearDown() {}

    /**
     *
     */
    public getExpectedExceptionMessage(): string {
        return this.expectedExceptionMessage;
    }

    /**
     *
     */
    public getExpectedException(): Function {
        return this.expectedException;
    }

    /**
     *
     * @param condition
     */
    public assertTrue(condition: boolean) {
        if (!condition) {
            throw new AssertionException('Could not assert that false is true');
        }
    }

    /**
     * @param expectedException
     * @param expectedExceptionMessage
     */
    protected expectException(expectedException: Function, expectedExceptionMessage: string = null) {
        this.expectedException = expectedException;
        this.expectedExceptionMessage = expectedExceptionMessage;
    }
}
