import {TestCase} from "../../src/Tests/TestCase";
import {AssertionException} from "../../src/Errors/AssertionException";

export class TestCaseTest extends TestCase
{
    private testCase: TestCase;

    /**
     * @inheritDoc
     */
    public setUp() {
        super.setUp();

        this.testCase = new TestCase();
    }

    /**
     *
     */
    public test_assertTrue_testTrue()
    {
        this.testCase.assertTrue(true);
    }

    /**
     *
     */
    public test_assertTrue_testFalse()
    {
        this.expectException(AssertionException, 'Could not assert that false is true');
        this.testCase.assertTrue(false);
    }
}